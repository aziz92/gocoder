Pod::Spec.new do |s|  
    s.name              = 'GoCoder'
    s.version           = '1.0.0'
    s.summary           = 'Pod for Wowza GoCoder SDK.'
    s.homepage          = 'http://wowza.com/'

    s.author            = { 'Aziz' => 'sdk@example.com' }
    s.license           = { :type => 'Apache-2.0', :file => 'LICENSE' }

    s.platform          = :ios
    s.source            = { :git => 'https://gitlab.com/aziz92/gocoder' }

    s.ios.deployment_target = '8.0'
    s.ios.vendored_frameworks = 'WowzaGoCoderSDK.framework'
end
